{

"header" : {
  "navbar" : {
    "UPLOAD" : "Upload",
    "new" : {
      "NEW" : "New",
      "PROJECT" : "Projects",
      "TASK" : "Task",
      "USER" : "User",
      "EMAIL" : "Email"
    },
    "NOTIFICATIONS" : "Notifications"
  }
},
"aside" : {
  "nav" : {
    "HEADER" : "导航",
    "DASHBOARD" : "仪表板",
    "CALENDAR" : "日历",
    "EMAIL" : "邮件",
    "WIDGETS" : "小部件",
    "components" : {
      "COMPONENTS" : "Components",
      "ui_kits" : {
        "UI_KITS" : "UI Kits",
        "BUTTONS" : "按钮",
        "ICONS" : "图标",
        "GRID" : "网格",
        "BOOTSTRAP" : "Bootstrap",
        "SORTABLE" : "Sortable",
        "PORTLET" : "Portlet",
        "TIMELINE" : "时间线",
        "VECTOR_MAP" : "Vector Map"
      },
      "table" : {
        "TABLE" : "表格",
        "TABLE_STATIC" : "Table static",
        "DATATABLE" : "Datatable",
        "FOOTABLE" : "Footable"
      },
      "form" : {
        "FORM" : "表单",
        "FORM_ELEMENTS" : "Form elements",
        "FORM_VALIDATION" : "Form validation",
        "FORM_WIZARD" : "Form wizard"
      },
      "CHART" : "图表",
      "pages" : {
        "PAGES" : "Pages",
        "PROFILE" : "Profile",
        "POST" : "Post",
        "SEARCH" : "Search",
        "INVOICE" : "Invoice",
        "LOCK_SCREEN" : "Lock screen",
        "SIGNIN" : "Signin",
        "SIGNUP" : "Signup",
        "FORGOT_PASSWORD" : "Forgot password",
        "404" : "404"
      }
    },
    "your_stuff" : {
      "YOUR_STUFF": "Your Stuff",
      "PROFILE" : "Profile",
      "DOCUMENTS" : "Documents"
    }
  },
  "MILESTONE" : "Milestone",
  "RELEASE" : "Release"
}

}
